<?xml version="1.0" encoding="utf-8"?>
<!--
     Copyright (C) 2023 BlissRoms Project
     SPDX-License-Identifier: Apache-2.0
-->
<resources>

    <!-- Colon separated list of package names that should be granted DND access -->
    <string name="config_defaultDndAccessPackages" translatable="false">com.google.android.gms:com.google.android.apps.wellbeing</string>
    
    <!-- The component to be the default supervisor profile owner [DO NOT TRANSLATE] -->
    <string name="config_defaultSupervisionProfileOwnerComponent" translatable="false">com.google.android.gms/.kids.account.receiver.ProfileOwnerReceiver</string>
    <string name="config_systemSupervision" translatable="false">com.google.android.gms.supervision</string>

    <!-- For GMS devices, default backup transport must be the GmsCore. -->
    <string name="def_backup_transport" translatable="false">com.google.android.gms/.backup.BackupTransportService</string>

    <!-- Package name for the contacts metadata syncadapter [DO NOT TRANSLATE] -->
    <string name="metadata_sync_pacakge" translatable="false">com.google.android.gms</string>

    <!-- Default autofill service to enable [DO NOT TRANSLATE] -->
    <string name="config_defaultAutofillService" translatable="false">com.google.android.gms/.autofill.service.AutofillService</string>

    <!-- Component name that accepts ACTION_SEND intents for nearby (proximity-based) sharing.
         Used by ChooserActivity. -->
    <string name="config_defaultNearbySharingComponent" translatable="false">com.google.android.gms/com.google.android.gms.nearby.sharing.ShareSheetActivity</string>

    <!-- Component name that accepts settings intents for saved devices.
         Used by FastPairSettingsFragment. -->
    <string translatable="false" name="config_defaultNearbyFastPairSettingsDevicesComponent">com.google.android.gms/com.google.android.gms.nearby.discovery.devices.SavedDevicesSettingsActivity</string>

    <!-- The package name of the default network recommendation app. -->
    <string name="config_defaultNetworkRecommendationProviderPackage" translatable="false">com.google.android.gms</string>

    <!-- For Factory Reset Protection -->
    <string name="config_persistentDataPackageName" translatable="false">com.google.android.gms</string>

    <!-- The package name for the fast pair provider application. [DO NOT TRANSLATE] -->
    <string name="config_systemCompanionDeviceProvider" translatable="false">com.google.android.gms</string>

    <!-- Apps that are authorized to access shared accounts -->
    <string name="config_appsAuthorizedForSharedAccounts" translatable="false">;com.android.vending;com.android.settings;</string>

    <!-- Flag indicating which package name can access DeviceConfig table [DO NOT TRANSLATE] -->
    <string name="config_deviceConfiguratorPackageName" translatable="false">com.google.android.gms</string>

    <!-- The name of the package that will hold the game service role. -->
    <string name="config_systemGameService" translatable="false">com.google.android.gms</string>
    
    <!-- The name of the package that will hold the system activity recognizer role. -->
    <string name="config_systemActivityRecognizer" translatable="false">com.google.android.gms</string>

    <!-- The name of the package that will handle updating the device management role. -->
    <string name="config_devicePolicyManagementUpdater" translatable="false">com.google.android.gms</string>

    <!-- Package name for the call-based number verification app -->
    <string name="platform_number_verification_package" translatable="false">com.google.android.gms</string>

    <!-- Package name for default QR Code Component -->
    <string name="config_defaultQrCodeComponent" translatable="false">com.google.android.gms/.mlkit.barcode.ui.PlatformBarcodeScanningActivityProxy</string>

    <!-- Enable doze mode -->
    <bool name="config_enableAutoPowerModes">true</bool>

    <!-- Enable overlay for all location components. -->
    <bool name="config_enableNetworkLocationOverlay">true</bool>
    <bool name="config_enableFusedLocationOverlay">true</bool>
    <bool name="config_enableGeocoderOverlay">true</bool>
    <bool name="config_enableGeofenceOverlay">true</bool>

    <!-- Flag to enable VVM3 visual voicemail -->
    <bool name="vvm3_enabled">true</bool>

    <!-- An array of packages that can make sound on the ringer stream in priority-only DND mode -->
    <string-array name="config_priorityOnlyDndExemptPackages" translatable="false">
        <item>com.android.dialer</item>
        <item>com.google.android.dialer</item>
        <item>com.android.server.telecom</item>
        <item>android</item>
        <item>com.android.systemui</item>
    </string-array>

    <!-- List containing the allowed install sources for accessibility service. -->
    <string-array name="config_accessibility_allowed_install_source" translatable="false">
        <item>com.android.vending</item>
    </string-array>

    <!-- The set of system packages on device that are queryable regardless of the contents of their
         manifest. -->
    <string-array name="config_forceQueryablePackages" translatable="false">
        <item>com.android.settings</item>
        <item>com.android.providers.settings</item>
        <item>com.android.vending</item>
        <item>com.google.android.gms</item>
    </string-array>

    <string-array name="config_integrityRuleProviderPackages" translatable="false">
        <item>com.android.vending</item>
        <item>com.google.android.gms</item>
    </string-array>

    <string-array name="config_disabledUntilUsedPreinstalledImes" translatable="false">
        <item>com.google.android.inputmethod.latin</item>
    </string-array>

    <string-array name="config_locationProviderPackageNames" translatable="false">
        <item>com.google.android.gms</item>
        <item>com.android.location.fused</item>
    </string-array>

    <string-array name="config_ephemeralResolverPackage" translatable="false">
        <item>com.google.android.gms</item>
    </string-array>

</resources>
