#
# Copyright (C) 2023 The BlissRoms Project
#
# SPDX-License-Identifier: Apache-2.0
#

# Essential Packages
# Carrier Services
PRODUCT_PACKAGES += \
    CarrierServices \

# Calculator
PRODUCT_PACKAGES += \
    CalculatorGooglePrebuilt \

# Clock
PRODUCT_PACKAGES += \
    PrebuiltDeskClockGoogle \
    GoogleClockOverlay \

# Contacts
PRODUCT_PACKAGES += \
    GoogleContacts \
    GoogleContactsOverlay \

# Dialer
PRODUCT_PACKAGES += \
    GoogleDialer \
    GoogleDialerOverlay \

# Messaging
PRODUCT_PACKAGES += \
    PrebuiltBugle \
    GoogleMessagesOverlay

# Digital Wellbeing
PRODUCT_PACKAGES += \
    WellbeingPrebuilt \
    DigitalWellbeingOverlay \

# Drive
PRODUCT_PACKAGES += \
    Drive \

# Maps
PRODUCT_PACKAGES += \
    LocationHistoryPrebuilt \
    Maps \
    GoogleLocationHistoryOverlay \

# Photos
PRODUCT_PACKAGES += \
    Photos \
    GooglePhotosOverlay \

# Search
PRODUCT_PACKAGES += \
    Velvet \
    VelvetOverlay

# Turbo
PRODUCT_PACKAGES += \
    TurboPrebuilt \
    DeviceHealthServicesOverlay \
    GMSConfigSettingsOverlayTurbo \
