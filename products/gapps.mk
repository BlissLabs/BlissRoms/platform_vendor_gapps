#
# Copyright (C) 2023 The BlissRoms Project
#
# SPDX-License-Identifier: Apache-2.0
#

# Default Packages
PRODUCT_PACKAGES += \
    ConfigUpdater \
    AndroidPlatformServices \
    GoogleContactsSyncAdapter \
    GoogleCalendarSyncAdapter \
    GoogleServicesFramework \
    MlkitBarcodeUIPrebuilt \
    Phonesky \
    PrebuiltGmsCoreSc \
    PrebuiltGmsCoreSc_AdsDynamite \
    PrebuiltGmsCoreSc_CronetDynamite \
    PrebuiltGmsCoreSc_DynamiteLoader \
    PrebuiltGmsCoreSc_DynamiteModulesA \
    PrebuiltGmsCoreSc_DynamiteModulesC \
    PrebuiltGmsCoreSc_GoogleCertificates \
    PrebuiltGmsCoreSc_MapsDynamite \
    PrebuiltGmsCoreSc_MeasurementDynamite \
    VisionBarcodePrebuilt \
    com.google.android.dialer.support

# RRO Overlays
PRODUCT_PACKAGES += \
    GMSCoreConfigOverlay \

# GMS Core
PRODUCT_PRODUCT_PROPERTIES += \
    ro.opa.eligible_device=true

# SetupWizard
PRODUCT_PACKAGES += \
    GoogleOneTimeInitializer \
    GoogleRestorePrebuilt-v445524 \
    PartnerSetupPrebuilt \
    SetupWizardPrebuilt \

# SetupWizard Props
PRODUCT_PRODUCT_PROPERTIES += \
    ro.setupwizard.enterprise_mode=1 \
    ro.setupwizard.rotation_locked=true \
    setupwizard.feature.baseline_setupwizard_enabled=true \
    setupwizard.enable_assist_gesture_training=true \
    setupwizard.theme=glif_v3_light \
    setupwizard.feature.skip_button_use_mobile_data.carrier1839=true \
    setupwizard.feature.show_pai_screen_in_main_flow.carrier1839=false \
    setupwizard.feature.show_pixel_tos=false

ifeq ($(TARGET_ESSENTIAL_GAPPS),true)
$(call inherit-product, vendor/gapps/products/essential.mk)
endif

ifeq ($(TARGET_STOCK_GAPPS),true)
$(call inherit-product, vendor/gapps/products/stock.mk)
endif

# Pixel Framework
$(call inherit-product-if-exists, vendor/pixel-framework/config.mk)

$(call inherit-product, vendor/gapps/common/common-vendor.mk)
